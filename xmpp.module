<?php

/**
 * @file
 * Extensible Messaging and Presence Protocol API.
 *
 * Allows Drupal components to send instant messages to users via the extensible
 * messaging and presence protocol.
 */

/**
 * Implementation of hook_help().
 */
function xmpp_help($section) {
  switch ($section) {
    case 'admin/help#xmpp':
      return '<p>' . t('
        The XMPP module provides other Drupal components with a way to
        communicate over the extensible messaging and presence protocol (aka
        Jabber). Once enabled and configured, other modules will be able to send
        instant messages to site users.
      ') . '</p>';
  }
}

/**
 * Implementation of hook_perm().
 */
function xmpp_perm() {
  return array('administer xmpp');
}

/**
 * Implementation of hook_enable().
 */
function xmpp_enable() {
  $secret = sha1(uniqid(mt_rand(), true));
  variable_set('xmpp_server_secret', $secret);
  variable_set('xmpp_client_auth_passwd', $secret);
}

/**
 * Implementation of hook_menu().
 */
function xmpp_menu($may_cache) {
  $items = array();

  if ($may_cache) {
    $items[] = array(
      'path'               => 'admin/settings/xmpp',
      'title'              => t('XMPP Settings'),
      'description'        => t('Configure XMPP Module.'),
      'callback'           => 'drupal_get_form',
      'callback arguments' => array('xmpp_client_settings'),
      'access'             => user_access('administer xmpp')
    );

    $items[] = array(
      'path'   => 'admin/settings/xmpp/client',
      'title'  => t('Client'),
      'type'   => MENU_DEFAULT_LOCAL_TASK,
      'access' => user_access('administer xmpp')
    );

    $items[] = array(
      'path'               => 'admin/settings/xmpp/server',
      'title'              => t('Server'),
      'callback'           => 'drupal_get_form',
      'callback arguments' => array('xmpp_server_settings'),
      'type'               => MENU_LOCAL_TASK,
      'access'             => user_access('administer xmpp')
    );
  }
  else {
    $items[] = array(
      'path'     => 'xmpp/server',
      'title'    => 'XMPP Server',
      'callback' => 'xmpp_server',
      'type'     => MENU_CALLBACK,
      'access'   => variable_get('xmpp_server_enabled', false)
    );
  }

  return $items;
}

/**
 * Implementation of hook_form_alter().
 */
function xmpp_form_alter($form_id, &$form) {
  if ($form_id == 'xmpp_client_settings')
    $form['#submit']['xmpp_client_settings_test'] = array();
}

/**
 * Client settings page.
 */
function xmpp_client_settings() {
  $form            = array();
  $fields          = array();
  $crypto_disabled = !extension_loaded('openssl');
  $crypto_text     = ($crypto_disabled ? ' (Requires OpenSSL extension)' : '');
  $crypto          = ($crypto_disabled ? 'none' : 'tls');
  $jid_format      = htmlspecialchars('<user>@<domain>[/<resource>]');

  $sql   = 'SELECT fid, title FROM {profile_fields} WHERE type = \'textfield\'';
  $query = db_query($sql);

  while ($record = db_fetch_array($query))
    $fields[$record['fid']] = $record['title'];

  $transport_methods = array(
    'tcp'    => t('External (TCP/IP)'),
    'webtcp' => t('Built-in (WebTCP)')
  );

  $crypto_methods = array(
    'none' => t('None'),
    'tls'  => t('TLS'),
    'ssl'  => t('SSL')
  );

  //
  // Basic settings
  //

  $form['client'] = array(
    '#type'        => 'fieldset',
    '#title'       => t('Basic settings')
  );

  $form['client']['xmpp_client_auth_jid'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Username'),
    '#description'   => t('JID to use for authentication ('.$jid_format.').'),
    '#default_value' => variable_get('xmpp_client_auth_jid', ''),
    '#required'      => true
  );

  $form['client']['xmpp_client_auth_passwd'] = array(
    '#type'          => 'password',
    '#title'         => t('Password'),
    '#description'   => t('Password to use for authentication. Leave this '.
                          'field blank to keep the old password or when using '.
                          'the built-in server.')
  );

  $form['client']['xmpp_client_jid_field'] = array(
    '#type'          => 'select',
    '#title'         => t('User JID Field'),
    '#description'   => t('Profile field which is used to store user JIDs. '.
                          'You should create this if it doesn\'t already '.
                          'exist.'),
    '#options'       => $fields,
    '#default_value' => variable_get('xmpp_client_jid_field', ''),
    '#required'      => true
  );

  //
  // Server type
  //

  $form['server_type'] = array(
    '#type'  => 'fieldset',
    '#title' => t('Server type'),
  );

  $form['server_type']['xmpp_client_transport'] = array(
    '#type'          => 'radios',
    '#title'         => t('Transport method'),
    '#description'   => t('Server type and transport method to be used.'),
    '#options'       => $transport_methods,
    '#default_value' => variable_get('xmpp_client_transport', 'tcp'),
    '#required'      => true
  );

  //
  // External (TCP/IP)
  //

  $form['external_tcp'] = array(
    '#type'        => 'fieldset',
    '#title'       => t('External (TCP/IP)'),
    '#collapsible' => true,
    '#collapsed'   => true
  );

  $form['external_tcp']['xmpp_client_tcp_host'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Host'),
    '#description'   => t('Hostname or IP of the external server. This can '.
                          'usually be determined automatically from the JID.'),
    '#default_value' => variable_get('xmpp_client_tcp_host', '')
  );

  $form['external_tcp']['xmpp_client_tcp_port'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Port'),
    '#description'   => t('Port on which to establish the connection. The '.
                          'default is 5222, 5223 if SSL is used.'),
    '#default_value' => variable_get('xmpp_client_tcp_port', ''),
    '#maxlength'     => 5
  );

  $form['external_tcp']['xmpp_client_tcp_use_dns'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Use DNS'),
    '#default_value' => variable_get('xmpp_client_tcp_use_dns', true),
    '#description'   => t('Enable DNS SRV look-ups.')
  );

  $form['external_tcp']['xmpp_client_tcp_crypto'] = array(
    '#type'          => 'radios',
    '#title'         => t('Encryption mechanism'. $crypto_text),
    '#description'   => t('Type of encryption to use (if available).'),
    '#default_value' => variable_get('xmpp_client_tcp_crypto', $crypto),
    '#options'       => $crypto_methods,
    '#disabled'      => $crypto_disabled
  );

  //
  // Built-in (WebTCP)
  //

  $form['builtin_webtcp'] = array(
    '#type'        => 'fieldset',
    '#title'       => t('Built-in (WebTCP)'),
    '#collapsible' => true,
    '#collapsed'   => true
  );

  $form['builtin_webtcp']['xmpp_client_webtcp_url'] = array(
    '#type'          => 'textfield',
    '#title'         => t('URL'),
    '#description'   => t('Location of the WebTCP server. Usually this is '.
                          'http://[your_domain]/[path_to_drupal]/xmpp/server. '.
                          'Do not add a trailing slash.'),
    '#default_value' => variable_get('xmpp_client_webtcp_url',
                                     url('xmpp/server', null, null, true))
  );

  //
  // Test configuration
  //

  $form['test'] = array(
    '#type'  => 'fieldset',
    '#title' => t('Test configuration'),
  );

  $form['test']['xmpp_client_test_jid'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Recipient'),
    '#description'   => t('Send a test IM to the specified JID.')
  );

  return system_settings_form($form);
}

/**
 * Validate client settings form.
 */
function xmpp_client_settings_validate($form_id, $values, $form) {

  if (!$values['xmpp_client_auth_passwd']) {

    if (!($passwd = variable_get('xmpp_client_auth_passwd', ''))) {
      form_set_error('', t('Please enter the password.'));
      return;
    }

    form_set_value($form['client']['xmpp_client_auth_passwd'], $passwd);
  }

  if (!xmpp_check_jid($values['xmpp_client_auth_jid'])) {
    form_set_error('', t('Please enter a valid JID.'));
    return;
  }

  if ($values['xmpp_client_test_jid']) {
    if (!xmpp_check_jid($values['xmpp_client_test_jid'])) {
      form_set_error('', t('Please enter a valid test JID.'));
      return;
    }
  }
}

/**
 * Submit client settings form.
 */
function xmpp_client_settings_test($form_id, $form) {

  variable_set('xmpp_client_configured', true);

  if (!($test_jid = $form['xmpp_client_test_jid']))
    return;

  if (!xmpp_message($test_jid, 'Hello, this is a test IM from Drupal :)')) {
    drupal_set_message('Error sending message: '. xmpp_last_error(), 'error');
    return;
  }

  drupal_set_message('Test message sent.');
}

/**
 * Server settings page.
 */
function xmpp_server_settings() {
  $form = array();

  $form['server'] = array(
    '#type'  => 'fieldset',
    '#title' => t('Server configuration'),
  );

  $form['server']['xmpp_server_enabled'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Enable server'),
    '#description'   => t('Turn the built-in server on or off.'),
    '#default_value' => variable_get('xmpp_server_enabled', false)
  );

  $form['server']['xmpp_server_stream_from'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Host'),
    '#description'   => t('Domain name hosted by this server.'),
    '#default_value' => variable_get('xmpp_server_stream_from', ''),
    '#required'      => true
  );

  $form['server']['xmpp_server_secret'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Secret'),
    '#description'   => t('Secret phrase needed to activate the server. This '.
                          'value needs to be set as the client\'s password '.
                          'in order for it to be able to access the server. '.
                          'By default, server is protected with a random sha1 '.
                          'hash string and you should only modify this field '.
                          'if you want other sites to use this server.'),
    '#default_value' => variable_get('xmpp_server_secret', ''),
    '#required'      => true
  );

  $form['server']['xmpp_server_webtcp_port_start'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Start port'),
    '#description'   => t('First port that can be used for incoming '.
                          'connections. Must be greater than 5269.'),
    '#default_value' => variable_get('xmpp_server_webtcp_port_start', '48004'),
    '#maxlength'     => 5,
    '#required'      => true
  );

  $form['server']['xmpp_server_webtcp_port_end'] = array(
    '#type'          => 'textfield',
    '#title'         => t('End port'),
    '#description'   => t('Last port that can be used for incoming '.
                          'connections. Must be greater than the start port.'),
    '#default_value' => variable_get('xmpp_server_webtcp_port_end', '48127'),
    '#maxlength'     => 5,
    '#required'      => true
  );

  return system_settings_form($form);
}

/**
 * Server node, responds to requests for /xmpp/server.
 */
function xmpp_server($secret = '') {

  if (!is_string($secret) || $secret !== variable_get('xmpp_server_secret', 0))
    return drupal_access_denied();

  require_once dirname(__FILE__).'/lib/server.inc';

  $conf = array(
    'stream.from'       => variable_get('xmpp_server_stream_from', ''),
    'webtcp.port.start' => variable_get('xmpp_server_webtcp_port_start', ''),
    'webtcp.port.end'   => variable_get('xmpp_server_webtcp_port_end', '')
  );

  try {
    $sess = XMPP_Server::instance()->listen($conf);

    while ($sess->ready)
      $sess->wait();
  }
  catch (Exception $ex) {
    if (isset($sess))
      $sess->handleException($ex);
  }
}

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// API
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

/**
 * Get a connected XMPP_Session object.
 */
function xmpp_get_session() {
  static $sess  = null;
  static $first = true;

  if ($first) {

    $first = false;

    if (!variable_get('xmpp_client_configured', false))
      return null;

    require_once dirname(__FILE__).'/lib/client.inc';

    $conf = array(
      'auth.jid'    => variable_get('xmpp_client_auth_jid', ''),
      'auth.passwd' => variable_get('xmpp_client_auth_passwd', ''),
      'transport'   => variable_get('xmpp_client_transport', '')
    );

    if ($conf['transport'] == 'tcp') {
      if ($host = variable_get('xmpp_client_tcp_host', ''))
        $conf['tcp.host'] = $host;

      if ($port = variable_get('xmpp_client_tcp_port', ''))
        $conf['tcp.port'] = $port;

      $conf['tcp.use_dns'] = variable_get('xmpp_client_tcp_use_dns', true);
      $crypto = variable_get('xmpp_client_tcp_crypto', 'none');

      if ($crypto == 'none')
        $conf['tcp.use_tls'] = false;

      if ($crypto == 'ssl')
        $conf['tcp.use_ssl'] = true;
    }
    else {
      $conf['webtcp.url']  = variable_get('xmpp_client_webtcp_url', '');
      $conf['webtcp.url'] .= '/'.variable_get('xmpp_client_auth_passwd', '');
    }

    try {
      $sess = XMPP_Client::instance()->connect($conf);
      $sess->Presence->set(1);
    }
    catch (Exception $ex) {
      $sess = null;
      xmpp_last_error($ex->getMessage());
    }
  }

  return ($sess && $sess->ready ? $sess : null);
}

/**
 * Map user id to jabber id.
 */
function xmpp_get_jid($uid = null) {

  static $jid_regex = '/^[^@]+@[^\/]+(?:\/.+)?$/';
  $fid = variable_get('xmpp_client_jid_field', '');

  if ($fid === '')
    return (is_array($uid) ? array() : '');

  if (is_null($uid)) {
    global $user;
    $uid = $user->uid;
  }

  $match  = (is_array($uid) ? 'uid IN ('.implode(',', $uid).')' : 'uid='.$uid);
  $result = array();
  $sql    = "SELECT uid, value FROM {profile_values} WHERE $match AND fid=%d";
  $query  = db_query($sql, $fid);

  while ($record = db_fetch_array($query)) {
    if (xmpp_check_jid($record['value']))
      $result[$record['uid']] = $record['value'];
  }

  return (is_array($uid) ? $result : ($result ? current($result) : ''));
}

/**
 * Send an instant message to the specified jid.
 */
function xmpp_message($jid, $message) {

  if (!xmpp_presence_subscribe($jid))
    return false;

  if (!($sess = xmpp_get_session()))
    return false;

  try {
    $sess->IM->send($jid, $message);
    return true;
  }
  catch (Exception $ex) {
    $sess->handleException($ex);
    xmpp_last_error($ex->getMessage());
    return false;
  }
}

/**
 * Subscribe to the presence status of a user.
 */
function xmpp_presence_subscribe($jid) {

  if (!xmpp_check_jid($jid) || !($sess = xmpp_get_session()))
    return false;

  list($local)  = explode('/', variable_get('xmpp_client_auth_jid', ''), 2);
  list($remote) = explode('/', $jid, 2);

  $sql   = 'SELECT * FROM {xmpp_presence} WHERE remote=\'%s\' AND local=\'%s\'';
  $query = db_query($sql, $remote, $local);

  if (db_fetch_array($query))
    return true;

  try {
    $sess->Presence->subscribe($remote);

    $sql   = 'INSERT INTO {xmpp_presence} VALUES (\'%s\', \'%s\')';
    $query = db_query($sql, $remote, $local);

    return true;
  }
  catch (Exception $ex) {
    $sess->handleException($ex);
    xmpp_last_error($ex->getMessage());
    return false;
  }
}

/**
 * Do not send any outgoing stanzas until xmpp_end_buffer is called.
 */
function xmpp_start_buffer() {

  if (!($sess = xmpp_get_session()))
    return false;

  $sess->stream->autosend = false;
  return true;
}

/**
 * Send all buffered stanzas and return to normal mode of operation.
 */
function xmpp_end_buffer() {

  if (!($sess = xmpp_get_session()))
    return false;

  try {
    $sess->stream->send();
    return true;
  }
  catch (Exception $ex) {
    $sess->handleException($ex);
    xmpp_last_error($ex->getMessage());
    return false;
  }
}

/**
 * Check basic JID syntax.
 */
function xmpp_check_jid($jid) {
  return preg_match('/^[^@]+@[^\/]+(?:\/.+)?$/', $jid);
}

/**
 * Get the last XMPP error message.
 */
function xmpp_last_error($set = null) {
  static $error = '';
  return ($set ? $error = t($set) : $error);
}
